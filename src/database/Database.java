package database;

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
	import java.util.List;
import java.util.Map;

import model.Book;
import model.Rent;
import model.User;
import view.DetailsBookView;
	 
	 
	public class Database {
		private static Database instance = null;	
		/* Static 'instance' method */
		public static Database getInstance() {
			if(instance == null) {
		         instance = new Database();
		      }
			return instance;
			
		}
	    public static final String DRIVER = "org.sqlite.JDBC";
	    public static final String DB_URL = "jdbc:sqlite:database.db";
	 
	    private Connection conn;
	    private Statement stat;
	 
	    private Database() {
	        try {
	            Class.forName(Database.DRIVER);
	        } catch (ClassNotFoundException e) {
	            System.err.println("No JDBC driver");
	            e.printStackTrace();
	        }
	 
	        try {
	            conn = DriverManager.getConnection(DB_URL);
	            stat = conn.createStatement();
	        } catch (SQLException e) {
	            System.err.println("connecting error");
	            e.printStackTrace();
	        }
	 
	        createTables();
	    }
	 
	    public boolean createTables()  {
	    	String createUser = "CREATE TABLE IF NOT EXISTS user (id_user INTEGER PRIMARY KEY AUTOINCREMENT, fname varchar(255), lname varchar(255), pesel int)";
	        String createBooks = "CREATE TABLE IF NOT EXISTS book (id_book INTEGER PRIMARY KEY AUTOINCREMENT, title varchar(255), author varchar(255), quantity int(3))";
	        String createRent = "CREATE TABLE IF NOT EXISTS rent (id_rent INTEGER PRIMARY KEY AUTOINCREMENT, id_book int, id_user int)";
	        try {
	            stat.execute(createBooks);
	            stat.execute(createRent);
	            stat.execute(createUser);
	        } catch (SQLException e) {
	            System.err.println("Error creating tables");
	            e.printStackTrace();
	            return false;
	        }
	        return true;
	    }
	    
	    public int insertUser(String fname, String lname, String pesel) {
	    	int ret;
	        try {
	            PreparedStatement prepStmt = conn.prepareStatement(
	                    "insert into user values (NULL, ?, ?, ?);");
	            prepStmt.setString(1, fname);
	            prepStmt.setString(2, lname);
	            prepStmt.setString(3, pesel);
	            prepStmt.execute();
	            ret = prepStmt.getGeneratedKeys().getInt(1);
	        } catch (SQLException e) {
	            System.err.println("Error - insert rent");
	            e.printStackTrace();
	            return 0;
	        }
	        return ret;
	    }
	 
	    public int insertBook(String title, String author, int quantity) {
	    	int ret;
	        try {
	            PreparedStatement prepStmt = conn.prepareStatement(
	                    "insert into book values (NULL, ?, ?, ?);");
	            prepStmt.setString(1, title);
	            prepStmt.setString(2, author);
	            prepStmt.setInt(3, quantity);
	            prepStmt.execute();
	            ret = prepStmt.getGeneratedKeys().getInt(1);
	        } catch (SQLException e) {
	            System.err.println("Error - insert book");
	            return 0;
	        }
	        return ret;
	    }
	 
	    public int insertRent(int id_book, int id_user) {
	    	System.out.println("temp");
	    	int ret;
	        try {
	            PreparedStatement prepStmt = conn.prepareStatement(
	                    "insert into rent values (NULL, ?, ?);");
	            prepStmt.setInt(1, id_book);
	            prepStmt.setInt(2, id_user);
	            prepStmt.execute();
	            ret = prepStmt.getGeneratedKeys().getInt(1);
	        } catch (SQLException e) {
	            System.err.println("Error - insert rent");
	            return 0;
	        }
	        return ret;
	    }
	    
	    
	    public Map<Integer, User> selectUsers() {
	    	Map<Integer, User> users = new HashMap<Integer, User>(); 
	    	int user_id=0;
	    	try {
	            ResultSet result = stat.executeQuery("SELECT * FROM user ORDER BY id_user ASC");
	            int id;
	            String fname, lname, pesel;
	            while(result.next()) {
	                id = result.getInt("id_user");
	                user_id=id;
	                fname = result.getString("fname");
	                lname = result.getString("lname");
	                pesel = result.getString("pesel");
	                users.put(id, new User(id, fname, lname, pesel));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	        User.setIds(user_id+1);
	        return users;
	    }
	 
	    public Map<Integer, Book> selectBooks() {
	    	Map<Integer, Book> books = new HashMap<Integer, Book>(); 
	    	int book_id=0;
	        try {
	            ResultSet result = stat.executeQuery("SELECT * FROM book ORDER BY id_book ASC");
	            int id, quantity;
	            String title, author;
	            while(result.next()) {
	                id = result.getInt("id_book");
	                book_id = id;
	                title = result.getString("title");
	                author = result.getString("author");
	                quantity = result.getInt("quantity");
	                books.put(id, new Book(id, author, title, quantity));
	                //todo Book.setID();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	        Book.setId(book_id+1);
	        return books;
	    }
	    
	    public Map<Integer, Rent> selectRent() {
	    	Map<Integer, Rent> rent = new HashMap<Integer, Rent>(); 
	        try {
	            ResultSet result = stat.executeQuery("SELECT * FROM rent ORDER BY id_rent ASC");
	            int id, ids, idr;
	            while(result.next()) {
	            	
	                id = result.getInt("id_book");
	                ids = result.getInt("id_user");
	                idr = result.getInt("id_rent");
	                Rent t = new Rent(idr, id, ids);
	                rent.put(idr, t);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        
	            return null;
	        }
	        return rent;
	    }
	    	    
	    //=========== UPDATE
	    public void updateUser(int id, String fname, String lname, String pesel) {
	    	String query = "UPDATE user SET fname = '"+fname+"', lname = '"+lname+"', pesel = '"+pesel+"' WHERE id_user = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public void updateBook(int id, String title, String author, int quantity) {
	    	String query = "UPDATE book SET title = '"+title+"', author = '"+author+"', quantity = '"+quantity+"' WHERE id_book = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public void updateRent(int id, int id_book, int id_user) {
	    	String query = "UPDATE rent SET id_book = '"+id_book+"', id_user = '"+id_user+"' WHERE id_rent = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    //============ DELETE
	    public Boolean deleteBook(int id) {
	    	String query = "DELETE FROM book WHERE id_book = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return false;
	        }
	    	return true;
	    }
	    
	    public Boolean deleteUser(int id) {
	    	String query = "DELETE FROM user WHERE id_user = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return false;
	        }
	    	return true;
	    }
	    public Boolean deleteRent(int id) {
	    	String query = "DELETE FROM rent WHERE id_rent = "+ id;
	    	try {
	            stat.executeUpdate(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return false;
	        }
	    	return true;
	    }
	    //========== CLOSE
	    public void closeConnection() {
	        try {
	            conn.close();
	        } catch (SQLException e) {
	            System.err.println("Error - close connection");
	            e.printStackTrace();
	        }
	    }
	}

