package controller;



import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import model.Book;
import model.Rent;
import model.User;
import view.DetailsBookView;
import view.DetailsUserView;
import view.MainView;
import view.RentView;
import view.UserView;
import view.BookView;

public class Controller implements Observer{
		private BookService bookService;
		private UserService userService;
		private RentService rentService;
		
		private Map<Integer, Rent> modelRent;
		private Map<Integer, Book> model;
		private Map<Integer, User> modelUser;
		
		 //private bookView view;
		static int prev=0;
		int idB = 0;
		
	   public Controller(Map<Integer, Book> _model, Map<Integer, User> _modelUser, Map<Integer, Rent> _modelRent){
	      this.model = _model;
	      this.modelUser = _modelUser;
	      this.modelRent = _modelRent;
	      //this.view = view;
	   }
	   
	   public void setBookService(BookService service) {
           this.bookService = service;
	   }
	   
	   public void setUserService(UserService service) {
           this.userService = service;
	   }
	   
	   public void setRentService(RentService service) {
	        this.rentService = service;
	   }
	   
	 //========= RENT
	   
		public void addModelRent(Rent r){
		   modelRent.put(r.getId(), r);
		}
		   
		public void deleteModelRent(int id){
		   modelRent.remove(id);
		}
		
		public void setRentBook(int id, int id_book) {
			// TODO Auto-generated method stub
			modelRent.get(id).setIdBook(id_book);
		}
		
		public void setRentUser(int id, int id_user) {
			// TODO Auto-generated method stub
			modelRent.get(id).setIdUser(id_user);
		}
		
	   //============= BOOKS
	   public void addModelBook(Book b){
		   model.put(b.getBookId(), b);
	   }
	   
	   
	   public void deleteModelBook(int id){
		   model.remove(id);
	   }
	   
	   public void setBookTitle(String title, int id){
	      ((Book) model.get(id)).setTitle(title);		
	   }

	   public void setBookAuthor(String author, int id){
	      ((Book) model.get(id)).setAuthor(author);		
	   }
	   
	   public void setBookQuantity(int quantity, int id){
		      ((Book) model.get(id)).setQuantity(quantity);		
	   }

	   //========= USERS
	   public void deleteModelUser(int id){
		   modelUser.remove(id);
	   }
	   
	   public void addModelUser(User u){
		   modelUser.put(u.getId(), u);
	   }
	   
	   public void setUserFname(String fname, int id){
		      modelUser.get(id).setFname(fname);		
	   }
	   
	   public void setUserLname(String lname, int id){
		      modelUser.get(id).setLname(lname);		
	   }
	   
	   public void setUserPesel(String pesel, int id){
		      modelUser.get(id).setPesel(pesel);		
	   }
	   
	   
	   //========= UPDATES
	   
	   public  void updateView(){				
		      UserView.getInstance().updateUsersView(modelUser);
	   }
	   public void updateView2(){
		   
		      BookView.getInstance().updateBookView(model);
	   }
	   public void updateViewRent(){				
		      RentView.getInstance().updateRentView(modelRent, model, modelUser);
	   }
	   
	public static void setPrev(int val){
		prev=val;
	}
	   
	   
	@Override
	public void update(Observable sender, Object arg) {
		
	if ((sender instanceof DetailsBookView)){
		 Map<String,String> data = (Map)arg;
		 bookService.update(data,this);
		 //update displayed data
		 updateView2();
 
	}else if ((sender instanceof UserView)){
		Map<String,Object> data = (Map)arg;
		if(data.get("type").equals("choose")){
			//show details or display users
			if(prev==1){
				User u = (User)data.get("user");
				Map<String, String> dataTemp = new HashMap<String, String>();
				dataTemp.put("type", "update");
				dataTemp.put("id_rent", "0");
				dataTemp.put("id_book", Integer.toString(idB));
				idB = 0;
				dataTemp.put("id_user", Integer.toString(u.getId()));
				rentService.update(dataTemp, this);
				updateViewRent();
				UserView.getInstance().close();
					//RentView.getInstance().display();
				
			} else{
					DetailsUserView.getInstance().display((User)data.get("user"));
			}	
		}else//display all users
		updateView();
		
	}else if ((sender instanceof BookView)){
		Map<String,Object> data = (Map)arg;
		//============== BOOKS -> CHOOSE
		if(data.get("type").equals("choose")){
			if(prev==1){//============= choose book to rent
				Book b = (Book)data.get("book");
				idB = b.getBookId();
				BookView.getInstance().close();
				UserView.getInstance().display();
			} else//================ choose book to edit
				DetailsBookView.getInstance().display((Book)data.get("book"));
		} else//============ display list of books
				updateView2();

	}else if ((sender instanceof DetailsUserView)){
		Map<String,String> data = (Map)arg;
		userService.update(data,this);
		updateView();
		
		
	}else if ((sender instanceof RentView)){
		if(arg!=null){
			Map<String,String> data = (Map)arg;
			if(data.get("type").equals("delete")){
				rentService.update(data, this);
			}
		}
		updateViewRent();
		prev = 1;
	}else if ((sender instanceof MainView)){
		prev = 0;
	}else{
		System.err.println("add what to do :)");
	}
         
	}
	
	
}
