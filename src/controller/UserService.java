package controller;

import java.util.Map;

import database.Database;

import model.User;

public class UserService {
	public User create(Map<String,String> data) {
		//insert into database
		Database dataBase = Database.getInstance();
		int ids = dataBase.insertUser(data.get("fname"), data.get("lname"), data.get("pesel"));
		if( ids !=0){
			//return new book
			return new User(ids, data.get("fname"), data.get("lname"), data.get("pesel"));
		}else{
			return null;	
		}
	}
	
	public void update(Map<String,String> data, Controller bc){
		int id = Integer.parseInt(data.get("id"));
		if(id==0){//add user
			if(!data.get("type").equals("delete"))
				bc.addModelUser(create(data));
			
		}else{//update user
			if(data.get("type").equals("update")){
		//update model
			bc.setUserFname(data.get("fname"), id);
			bc.setUserLname(data.get("lname"), id);
			bc.setUserPesel(data.get("pesel"), id);
		//update database
			Database.getInstance().updateUser(id, data.get("fname"), data.get("lname"), data.get("pesel"));
			
			}else if(data.get("type").equals("delete")){
				if(id>0){
					if(Database.getInstance().deleteUser(id))
						bc.deleteModelUser(id);
				
				}
				
			}
		}
		 
	}
}
