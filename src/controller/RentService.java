package controller;

import java.util.Map;

import database.Database;
import model.Rent;
import model.User;

public class RentService {
	
	public Rent create(Map<String,String> data) {
		//insert into database
		Database dataBase = Database.getInstance();
		int ids = dataBase.insertRent(validate(data.get("id_book")), validate(data.get("id_user")));
		if(ids !=0){
			//return new book
			return new Rent(ids, validate(data.get("id_book")), validate(data.get("id_user")));
		}else{
			return null;	
		}
	}
	
	public void update(Map<String,String> data, Controller bc){
		int id = validate(data.get("id_rent"));
		if(id==0){//add rent
			if(!data.get("type").equals("delete"))
				bc.addModelRent(create(data));
		}else{//update rent
			if(data.get("type").equals("update")){
		//update model
			bc.setRentBook(validate(data.get("id_book")), id);
			bc.setRentUser(validate(data.get("id_user")), id);
		//update database
			Database.getInstance().updateRent(id,validate(data.get("id_book")),validate(data.get("id_user")));
			
			}else if(data.get("type").equals("delete")){
				if(id>0){
					if(Database.getInstance().deleteRent(id))
						bc.deleteModelRent(id);
				
				}
				
			}
		}
		 
	}
	public int validate(String temp){
		if(temp.matches("-?\\d+(\\.\\d+)?"))//validate
			return Integer.parseInt(temp);
		else
			return 0;
	}
}
