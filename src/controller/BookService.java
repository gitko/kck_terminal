package controller;

import java.util.Map;

import database.Database;
import model.Book;
public class BookService {
	
	public Book create(Map<String,String> data) {
		//insert into database
		Database dataBase = Database.getInstance();
		int quantity = validate(data.get("quantity"));
		int ids = dataBase.insertBook(data.get("author"), data.get("title"), quantity);
		if(ids != 0){
			//return new book
			return new Book(ids, data.get("author"), data.get("title"), quantity);
		}else{
			return null;	
		}
	}
	
	public void update(Map<String,String> data, Controller bc){
		int id = validate(data.get("id"));
		if(id==0){//add book
			if(!data.get("type").equals("delete"))
				bc.addModelBook(create(data));
			
		}else{//update book
			if(data.get("type").equals("update")){
		//update model
			
			int quantity = validate(data.get("quantity"));
			bc.setBookAuthor(data.get("author"), id);
			bc.setBookTitle(data.get("title"), id);
			bc.setBookQuantity(quantity, id);
		//update database
			Database.getInstance().updateBook(id, data.get("title"), data.get("author"), quantity);
			
			}else if(data.get("type").equals("delete")){
				if(id>0){
					if(Database.getInstance().deleteBook(id))
						bc.deleteModelBook(id);
				
				}
				
			}
		}
		 
	}
	
	public int validate(String temp){
		if(temp.matches("-?\\d+(\\.\\d+)?"))//validate
			return Integer.parseInt(temp);
		else
			return 0;
	}
}
/*
if(Integer.parseInt(data.get("id"))==0){//add book
b = service.create(data); 
model.put(b.getBookId(), b);
System.out.println("Created new book");
}else{//update book

//System.out.println("Updated book");
service.update(data,this);
}
*/