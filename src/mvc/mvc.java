package mvc;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import controller.BookService;
import controller.RentService;
import controller.UserService;
import controller.Controller;
import database.Database;
import model.Book;
import model.Rent;
import model.User;
import view.DetailsBookView;
import view.DetailsUserView;
import view.MainView;
import view.RentView;
import view.UserView;
import view.View;
import view.BookView;

public class mvc {
		public static void main(String[] args) throws IOException, InterruptedException {
			
	      //fetch student record based on his roll no from the database
		  Map<Integer, Book> model  = retriveBookFromDatabase();
		  Map<Integer, User> modelUser  = retriveUserFromDatabase();
		  Map<Integer, Rent> modelRent  = retriveRentFromDatabase();
		  
	      //create controllers
	      Controller Controller = new Controller(model, modelUser, modelRent);
	    
	      
	      //set book services
	      Controller.setBookService(new BookService());
	      Controller.setUserService(new UserService());
	      Controller.setRentService(new RentService());
	      
	      //add observers
	      View.getInstance();
	      UserView.getInstance().addObserver(Controller);
	      DetailsBookView.getInstance().addObserver(Controller);
	      BookView.getInstance().addObserver(Controller);
	      DetailsUserView.getInstance().addObserver(Controller);
	      RentView.getInstance().addObserver(Controller);
	      MainView.getInstance().addObserver(Controller);
	      
	      //start main window
	      MainView.getInstance().display();
	      
	      //exit
	      System.out.println("database close connection");
	      Database data = Database.getInstance();
	      data.closeConnection();
          
	   }

	   private static Map<Integer, Book> retriveBookFromDatabase(){
		  Database data = Database.getInstance();
		  //data.insertBook("sienkiewicz", "undressed", 1);
		  //data.insertBook("kochan", "fraszka", 1);
		  
		  Map<Integer, Book>listOfBooks = new HashMap<Integer, Book>(); 
		  
		  //get data from database
		  listOfBooks = data.selectBooks();
	      
	      return listOfBooks;
	   }
	   
	   private static Map<Integer, User> retriveUserFromDatabase(){
		   Database data = Database.getInstance();
		   //data.insertUser("Filip", "Nowicki", "9304");
		   //data.insertUser("kochan", "fraszka", "454545");
			  
		   Map<Integer, User>listOfUsers = new HashMap<Integer, User>(); 
			  
		   //get data from database
		   listOfUsers = data.selectUsers();
		   return listOfUsers;
	   }
	   
	   private static Map<Integer, Rent> retriveRentFromDatabase(){
		   Database data = Database.getInstance();
		   //data.insertRent(1,2);
			  
		   Map<Integer, Rent>listOfRents = new HashMap<Integer, Rent>(); 
			  
		   //get data from database
		   listOfRents = data.selectRent();
		   return listOfRents;
	   }
}
