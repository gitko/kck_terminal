package model;

public class Book {
	 private int bookId;
	 private String author;
	 private String title;
	 private int quantity;
	 private static int _id = 1;
	public Book(String _author, String _title, int _quantity){
		this.bookId=_id++;
		this.author=_author;
		this.title=_title;
		this.setQuantity(_quantity);
	}
	public Book(int ids, String _author, String _title, int _quantity){
		this.bookId=ids;
		this.author=_author;
		this.title=_title;
		this.setQuantity(_quantity);
	}
	public static void setId(int ids){
		Book._id = ids;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
