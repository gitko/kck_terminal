package model;

public class User {
    private int id;
    private String fname;
    private String lname;
    private String pesel;
    private static int _id = 1;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFname() {
        return fname;
    }
    public void setFname(String imie) {
        this.fname = imie;
    }
    public String getLname() {
        return lname;
    }
    public void setLname(String nazwisko) {
        this.lname = nazwisko;
    }
    public String getPesel() {
        return pesel;
    }
    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
    public static void setIds(int ids){
		User._id = ids;
	}
    public User() { }
    public User(int id, String imie, String nazwisko, String pesel) {
        this.id = id;
        this.fname = imie;
        this.lname = nazwisko;
        this.pesel = pesel;
    }
    public User(String imie, String nazwisko, String pesel) {
        this.id = _id++;
        this.fname = imie;
        this.lname = nazwisko;
        this.pesel = pesel;
    }
    @Override
    public String toString() {
        return "["+id+"] - "+fname+" "+lname+" - "+pesel;
    }
}
