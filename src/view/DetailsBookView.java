package view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;
import com.googlecode.lanterna.gui2.TextBox;

import model.Book;

public class DetailsBookView  extends Observable{
	
	private static DetailsBookView instance = null;	
	
	public static DetailsBookView getInstance(){
		if(instance == null) {
	         instance = new DetailsBookView();
	      }
		return instance;
		
	}
	
	final BasicWindow window = new BasicWindow("Book details");
	final Label LabelAuthor = new Label("");
	final Label LabelTitle = new Label("");
	final Label LabelQuantity = new Label("");
	final TextBox textAuthor = new TextBox();
	final TextBox textTitle = new TextBox();
	final TextBox textQuantity = new TextBox();
	final TextBox textId = new TextBox().setReadOnly(true);
	
	private DetailsBookView(){
		Panel mainPanel = new Panel();
		mainPanel.setPreferredSize(new TerminalSize(80, 20));
		Panel panel = new Panel();
		panel.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
				//===================================================
				//=================================================== create book detalis window
				//===================================================
				Panel temp = new Panel();
		
				temp.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
				new Label("Id:").setPreferredSize(new TerminalSize(12, 1)).addTo(temp);
				textId.setPreferredSize(new TerminalSize(12, 1)).addTo(temp);
				new Label("").setPreferredSize(new TerminalSize(12, 1)).addTo(temp);
				
				new Label("Author:").addTo(temp);
				LabelAuthor.addTo(temp);
				textAuthor.addTo(temp);
				
				new Label("Title:").addTo(temp);
				LabelTitle.addTo(temp);
				textTitle.addTo(temp);
				
				new Label("Quantity:").addTo(temp);
				LabelQuantity.addTo(temp);
				textQuantity.addTo(temp);
				
				//===================================================
				//=================================================== buttons in book detalis window
				//===================================================
				//==================== BACK BUTTON 
				Button backBtn = new Button("BACK", new Runnable() {
					@Override
			        public void run() {View.getInstance().textGUI.removeWindow(window);}
				});
				panel.addComponent(backBtn.withBorder(Borders.doubleLine()));
				
				
				//==================== save
				Button saveBtn = new Button("save", new Runnable() {
					@Override
			        public void run() {saveData();}
				});
				panel.addComponent(saveBtn.withBorder(Borders.doubleLine()));
				
				
				//==================== delete
				Button deleteBtn = new Button("delete", new Runnable() {
					@Override
			        public void run() {deleteData();}
				});
				panel.addComponent(deleteBtn.withBorder(Borders.doubleLine()));
				
				//=================== add panels to main
				mainPanel.addComponent(temp);
				mainPanel.addComponent(panel.withBorder(Borders.doubleLine()));
				
				//=================== add main to window
				window.setComponent(Panels.vertical(mainPanel));
	}
	
	public void deleteData(){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "delete");
		data.put("id", textId.getText());
		
		// generate the event
		setChanged();
		notifyObservers(data);
			
		View.getInstance().textGUI.removeWindow(window);
		BookView.getInstance().display();
	}
	public void saveData(){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "update");
		data.put("id", textId.getText());
		data.put("author", textAuthor.getText());
		data.put("title", textTitle.getText());
		data.put("quantity", textQuantity.getText());
    // generate the event
		setChanged();
		notifyObservers(data);
		View.getInstance().textGUI.removeWindow(window);
		
		
	}
	public void display(Book book) {
		// TODO Auto-generated method stub
		LabelAuthor.setText(book.getAuthor());
		LabelTitle.setText(book.getTitle());
		textId.setText(new Integer(book.getBookId()).toString());
		LabelQuantity.setText(new Integer(book.getQuantity()).toString());
		textAuthor.setText("");
		textTitle.setText("");
		textQuantity.setText("");
		//display
		View.getInstance().textGUI.addWindowAndWait(window);
	}

	public void display() {
		// TODO Auto-generated method stub
		//clean
		LabelAuthor.setText("");
		LabelTitle.setText("");
		textId.setText("0");
		LabelQuantity.setText("");
		textAuthor.setText("");
		textTitle.setText("");
		textQuantity.setText("");
		//display
		View.getInstance().textGUI.addWindowAndWait(window);
	}

}
