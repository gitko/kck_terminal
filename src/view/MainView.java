package view;

import java.io.IOException;
import java.util.Observable;


import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.AnimatedLabel;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.EmptySpace;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.GridLayout.Alignment;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LayoutData;
import com.googlecode.lanterna.gui2.LayoutManager;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;



public class MainView extends Observable{
private static MainView instance = null;	
	
	public static MainView getInstance() throws IOException, InterruptedException{
		if(instance == null) {
	         instance = new MainView();
	      }
		return instance;
		
	}
	final BasicWindow window = new BasicWindow("Menu");
		
	
private MainView() throws IOException{
		
	//==================== USERS BUTTON 
	Button buttonUsers = new Button("USERS", new Runnable() {
		@Override
        public void run() {
			//textGUI.removeWindow(window);
			UserView.getInstance().display();
        }
	});
	
	//==================== BOOKS BUTTON 
	Button buttonBooks = new Button("BOOKS", new Runnable() {
		@Override
        public void run() {
			//textGUI.removeWindow(window);
			BookView.getInstance().display();
        }
	});
	
	//==================== RENTS BUTTON 
	Button buttonRent = new Button("RENTS", new Runnable() {
		@Override
        public void run() {
			//textGUI.removeWindow(window);
			RentView.getInstance().display();
        }
	});
	
	//==================== BOOKS BUTTON 
	Button buttonExit = new Button("EXIT", new Runnable() {
		@Override
        public void run() {
			View.getInstance().textGUI.removeWindow(window);
            System.exit(0);
        }
	});
		
	AnimatedLabel al = new AnimatedLabel("Welcome");
	al.addFrame("to the library");
	al.setPreferredSize(new TerminalSize(12, 1));
	al.addStyle(SGR.BOLD);
	al.startAnimation(600);
	
	
	Panel animatedPanel = new Panel();
	animatedPanel.setLayoutManager(new GridLayout(40));
	for(int i=0;i<40;i++)
		animatedPanel.addComponent(AnimatedLabel.createClassicSpinningLine(100));
	
	
	 Panel leftGridPanel = new Panel();
	 leftGridPanel.setPreferredSize(new TerminalSize(80, 20));
     leftGridPanel.setLayoutManager(new GridLayout(4).setVerticalSpacing(1));
     
     leftGridPanel.addComponent(al
             .setLayoutData(GridLayout.createLayoutData(GridLayout.Alignment.CENTER, GridLayout.Alignment.CENTER, true, false, 4, 1)));

     leftGridPanel.addComponent(new EmptySpace(TextColor.ANSI.BLACK, new TerminalSize(4, 1))
             .setLayoutData(GridLayout.createLayoutData(GridLayout.Alignment.FILL, GridLayout.Alignment.CENTER, true, false, 4, 1)));
     
     
     leftGridPanel.addComponent(buttonRent);
     leftGridPanel.addComponent(buttonUsers);
     leftGridPanel.addComponent(buttonBooks);
     leftGridPanel.addComponent(buttonExit);
     
     window.setFocusedInteractable(buttonRent);
     window.setComponent(Panels.vertical(animatedPanel,leftGridPanel));
	
}



	public void display() {
		// TODO Auto-generated method stub
		setChanged();
		notifyObservers();
		window.getPreferredSize();
		View.getInstance().textGUI.addWindowAndWait(window);	
		
	}


	

}
