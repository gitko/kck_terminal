package view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;

import controller.Controller;
import model.User;

public class UserView extends Observable{
private static UserView instance = null;	
	
	public static UserView getInstance(){
		if(instance == null) {
	         instance = new UserView();
	      }
		return instance;
		
	}
	final protected BasicWindow window = new BasicWindow("List of users");
	final protected Panel panel = new Panel();
	final protected Panel temp = new Panel();
	final protected Panel mainPanel = new Panel();
	
	
			//======================================
			//create table of users
			//======================================
			public void updateUsersView(final Map<Integer, User> users) {
						panel.removeAllComponents();
						temp.removeAllComponents();
						temp.setLayoutManager(new GridLayout(3));	
						//title
						
						new Label("First Name").addTo(temp).setPreferredSize(new TerminalSize(20, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
						new Label("Last Name").addTo(temp).setPreferredSize(new TerminalSize(20, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
						new Label("").addTo(temp).setPreferredSize(new TerminalSize(12, 1));
						for(final Integer key : users.keySet()){
							new Label(users.get(key).getFname()).addTo(temp);
							new Label(users.get(key).getLname()).addTo(temp);
							Button t = new Button("Choose", new Runnable() {
								@Override
								public void run() {
									Map<String, Object> data = new HashMap<String, Object>();
									data.put("type", "choose");
									data.put("user", users.get(key));
									data.put("window", window);
									// generate the event
											setChanged();
											notifyObservers(data);
											
								}
								}).addTo(temp);
							window.setFocusedInteractable(t);
						}
					panel.addComponent(temp.withBorder(Borders.singleLine()));	
					mainPanel.setSize(panel.getPreferredSize());
			}
						
	private UserView() {
		
		Panel panel2 = new Panel();
		
		panel2.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
		
		
		//==================== BACK BUTTON 
		Button buttonBack = new Button("BACK", new Runnable() {
			@Override
	        public void run() {View.getInstance().textGUI.removeWindow(window);}
		});
		panel2.addComponent(buttonBack.withBorder(Borders.doubleLine()));
		//==================== ADD USER BUTTON 
		Button button = new Button("Add user", new Runnable() {
			@Override
	        public void run() {
				DetailsUserView.getInstance().display();
	        }
		});
		panel2.addComponent(button.withBorder(Borders.doubleLine()));
		
		//=================== add panels to main
		mainPanel.addComponent(panel2);
		mainPanel.addComponent(panel.withBorder(Borders.doubleLine()));
				
		//=================== add main to window
		window.setComponent(Panels.vertical(mainPanel));
	}
	
	
	
	
	public void display() {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "display");
		setChanged();
		notifyObservers(data);
		View.getInstance().textGUI.addWindowAndWait(window);
	}
	public void close() {
		View.getInstance().textGUI.removeWindow(window);
	}	
	
}
