package view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;

import controller.Controller;
import model.Book;
import model.Rent;
import model.User;

public class RentView extends Observable {
	private static RentView instance = null;

	public static RentView getInstance() {
		if (instance == null) {
			instance = new RentView();
		}
		return instance;

	}

	final protected BasicWindow window = new BasicWindow("List of rents");
	final protected Panel panel = new Panel();
	final protected Panel temp = new Panel();
	final protected Panel mainPanel = new Panel();

	// ======================================
	// create table of rents
	// ======================================
	public void updateRentView(final Map<Integer, Rent> rents, final Map<Integer, Book> books, final Map<Integer, User> users) {
		temp.removeAllComponents();
		panel.removeAllComponents();
		temp.setLayoutManager(new GridLayout(5).setHorizontalSpacing(6));
		
		new Label("First name").addTo(temp).setPreferredSize(new TerminalSize(12, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("Last name").addTo(temp).setPreferredSize(new TerminalSize(12, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("Author").addTo(temp).setPreferredSize(new TerminalSize(12, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("Title").addTo(temp).setPreferredSize(new TerminalSize(12, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("").addTo(temp).setPreferredSize(new TerminalSize(10, 1));
		
		for (final Integer key : rents.keySet()) {
			User tmp = users.get(rents.get(key).getIdUser());
			new Label((tmp != null) ? tmp.getFname() : "deleted").addTo(temp);
			new Label((tmp != null) ? tmp.getLname() : "deleted").addTo(temp);
			Book tmp2 = books.get(rents.get(key).getIdBook());
			new Label((tmp2 != null) ? tmp2.getAuthor() : "deleted").addTo(temp);
			new Label((tmp2 != null) ? tmp2.getTitle() : "deleted").addTo(temp);
			Button t = new Button("Delete", new Runnable() {
				@Override
				public void run() {
					deleteData(rents.get(key).getId());
												
				}
				}).addTo(temp);
			window.setFocusedInteractable(t);
		}
		panel.addComponent(temp.withBorder(Borders.singleLine()));	
		mainPanel.setSize(panel.getPreferredSize());
	}

	private RentView() {
		
		Panel panel2 = new Panel();
		
		panel2.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
		
		//==================== BACK BUTTON 
		Button buttonBack = new Button("BACK", new Runnable() {
			@Override
	        public void run() {View.getInstance().textGUI.removeWindow(window);
	        Controller.setPrev(0);
			}
		});
		panel2.addComponent(buttonBack.withBorder(Borders.doubleLine()));
		
		// ==================== ADD RENT BUTTON
		Button buttonAddRent = new Button("Add rent", new Runnable() {
			@Override
	        public void run() {
				BookView.getInstance().display();
	        }
		});
		panel2.addComponent(buttonAddRent.withBorder(Borders.doubleLine()));
		
		//=================== add panels to main
		
		mainPanel.addComponent(panel2);
		mainPanel.addComponent(panel.withBorder(Borders.doubleLine()));
				
		//=================== add main to window
		window.setComponent(Panels.vertical(mainPanel));
	}
	public void deleteData(int id){
		Map<String, String> data = new HashMap<String, String>();
		data.put("type", "delete");
		data.put("id_rent", Integer.toString(id));
		// generate the event
		setChanged();
		notifyObservers(data);
		
	}
	public void display() {
		setChanged();
		notifyObservers();
		View.getInstance().textGUI.addWindowAndWait(window);
	}
}
