package view;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.TextGUI.Listener;
import com.googlecode.lanterna.gui2.TextGUIThread;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalFactory;
import com.googlecode.lanterna.terminal.swing.*;

public  class View extends Observable {
	private static View instance = null;	
	
	public static View getInstance(){
		if(instance == null) {
	         instance = new View();
	      }
		return instance;
		
	}
	 SwingTerminal terminal = new SwingTerminal();
	 Screen screen;
	 MultiWindowTextGUI textGUI;
	
	public View(){
		try {
			screen = new TerminalScreen(terminal);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		textGUI = new MultiWindowTextGUI(screen);
		
		
		JFrame frame = new JFrame();
		
		frame.add(terminal);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setSize(800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		try {
			screen.startScreen();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
		textGUI.setBlockingIO(false);
		textGUI.setEOFWhenNoWindows(true);
		textGUI.isEOFWhenNoWindows();
    
	}
    
    
}
