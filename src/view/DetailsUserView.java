package view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;
import com.googlecode.lanterna.gui2.TextBox;
import com.googlecode.lanterna.gui2.Window;

import model.Book;
import model.User;

public class DetailsUserView extends Observable{
private static DetailsUserView instance = null;	
	
	public static DetailsUserView getInstance(){
		if(instance == null) {
	         instance = new DetailsUserView();
	      }
		return instance;
		
	}
	
	
	final BasicWindow window = new BasicWindow("User details");
	final Label LabelFname = new Label("");
	final Label LabelLname = new Label("");
	final Label LabelPesel = new Label("");
	final TextBox textFname = new TextBox();
	final TextBox textLname = new TextBox();
	final TextBox textPesel = new TextBox();
	final TextBox textId = new TextBox().setReadOnly(true);
	
	public DetailsUserView() {
		Panel mainPanel = new Panel();
		mainPanel.setPreferredSize(new TerminalSize(80, 20));
		Panel panel = new Panel();
		panel.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
		
		//===================================================
		//=================================================== create book detalis window
		//===================================================
		Panel temp = new Panel();
		
		temp.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
	
		new Label("Id:").setPreferredSize(new TerminalSize(12, 1)).addTo(temp);
		textId.setPreferredSize(new TerminalSize(12, 1)).addTo(temp);
		new Label("").setPreferredSize(new TerminalSize(12, 1)).addTo(temp);

		new Label("First name:").addTo(temp);
		LabelFname.addTo(temp);
		textFname.addTo(temp);

		new Label("Last name:").addTo(temp);
		LabelLname.addTo(temp);
		textLname.addTo(temp);

		new Label("Pesel:").addTo(temp);
		LabelPesel.addTo(temp);
		textPesel.addTo(temp);

		//===================================================
		//=================================================== buttons in book detalis window
		//===================================================
		//==================== BACK BUTTON 
		Button backBtn = new Button("BACK", new Runnable() {
			@Override
	        public void run() {View.getInstance().textGUI.removeWindow(window);}
		});
		panel.addComponent(backBtn.withBorder(Borders.doubleLine()));
		
		//==================== save
		Button saveBtn = new Button("save", new Runnable() {
			@Override
	        public void run() {saveData();}
		});
		panel.addComponent(saveBtn.withBorder(Borders.doubleLine()));

		//delete
		//==================== delete
		Button deleteBtn = new Button("delete", new Runnable() {
			@Override
	        public void run() {deleteData();}
		});
		panel.addComponent(deleteBtn.withBorder(Borders.doubleLine()));
		
		//=================== add panels to main
		mainPanel.addComponent(temp);
		mainPanel.addComponent(panel.withBorder(Borders.doubleLine()));
		
		//=================== add main to window
		window.setComponent(Panels.vertical(mainPanel));
	}
	

	public void deleteData(){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "delete");
		data.put("id", textId.getText());
		// generate the event
		setChanged();
		notifyObservers(data);
		
		View.getInstance().textGUI.removeWindow(window);
		UserView.getInstance().display();
	}
	public void saveData(){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "update");
		data.put("id", textId.getText());
		data.put("fname", textFname.getText());
		data.put("lname", textLname.getText());
		data.put("pesel", textPesel.getText());
    // generate the event
		setChanged();
		notifyObservers(data);
		View.getInstance().textGUI.removeWindow(window);	
	}
	
	public void display(User user) {
		// TODO Auto-generated method stub
		LabelFname.setText(user.getFname());
		LabelLname.setText(user.getLname());
		textId.setText(new Integer(user.getId()).toString());
		LabelPesel.setText(user.getPesel());
		textFname.setText("");
		textLname.setText("");
		textPesel.setText("");
		//display
		View.getInstance().textGUI.addWindowAndWait(window);
	}

	public void display() {
		//clean
		LabelFname.setText("");
		LabelLname.setText("");
		textId.setText("0");
		LabelPesel.setText("");
		textFname.setText("");
		textLname.setText("");
		textPesel.setText("");
		//display
		View.getInstance().textGUI.addWindowAndWait(window);
	}

}
