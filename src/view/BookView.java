package view;

import java.io.IOException;
import java.util.*;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.AnimatedLabel;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Interactable;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LayoutManager;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

/*
import com.googlecode.lanterna.gui.*;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.EmptySpace;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Panel.Orientation;
import com.googlecode.lanterna.gui.component.Table;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.gui.layout.VerticalLayout;
import com.googlecode.lanterna.terminal.TerminalSize;
*/
import model.Book;
import model.User;


public class BookView extends Observable {

	private static BookView instance = null;	
	
	public static BookView getInstance(){
		if(instance == null) {
	         instance = new BookView();
	      }
		return instance;
		
	}
	final protected BasicWindow window = new BasicWindow("List of books");
	final protected Panel panel = new Panel();
	final protected Panel temp = new Panel();
	final protected Panel mainPanel = new Panel();
	//======================================
	//create table of books
	//======================================
	public void updateBookView(final Map<Integer, Book> books) { 
		temp.removeAllComponents();
		panel.removeAllComponents();
		temp.setLayoutManager(new GridLayout(3));	
		
		new Label("Author").addTo(temp).setPreferredSize(new TerminalSize(20, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("Title").addTo(temp).setPreferredSize(new TerminalSize(20, 1)).setBackgroundColor(new TextColor.RGB(115, 110, 100));
		new Label("").addTo(temp).setPreferredSize(new TerminalSize(12, 1));
		
				for(final Integer key : books.keySet()){
					new Label(books.get(key).getAuthor()).addTo(temp);
					new Label(books.get(key).getTitle()).addTo(temp);
					Button t = new Button("Choose", new Runnable() {
						@Override
						public void run() {
							
							Map<String, Object> data = new HashMap<String, Object>();
							data.put("type", "choose");
							data.put("book", books.get(key));
							data.put("window", window);
							// generate the event
									setChanged();
									notifyObservers(data);
									
						}
						}).addTo(temp);
					window.setFocusedInteractable(t);
					
				}
				panel.addComponent(temp.withBorder(Borders.singleLine()));	
				mainPanel.setSize(panel.getPreferredSize());
		
	}
	
	//======================================
	//init view
	//======================================
	private BookView(){
		
		Panel panel2 = new Panel();
		
		panel2.setLayoutManager(new GridLayout(3).setHorizontalSpacing(6));
		
		//==================== BACK BUTTON 
		Button buttonBack = new Button("BACK", new Runnable() {
			@Override
	        public void run() {View.getInstance().textGUI.removeWindow(window);}
		});
		panel2.addComponent(buttonBack.withBorder(Borders.doubleLine()));
		
		
		//==================== ADD BOOK BUTTON
		Button button = new Button("Add book", new Runnable() {
			@Override
	        public void run() {
					DetailsBookView.getInstance().display();
	        }
		});
		panel2.addComponent(button.withBorder(Borders.doubleLine()));
		
		//=================== add panels to main
		
		mainPanel.addComponent(panel2);
		mainPanel.addComponent(panel.withBorder(Borders.doubleLine()));
		
		//=================== add main to window
		window.setComponent(Panels.vertical(mainPanel));
	
		}

	public void display() {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("type", "display");
		setChanged();
		notifyObservers(data);
		View.getInstance().textGUI.addWindowAndWait(window);
		
	}
	
	public void close() {
		View.getInstance().textGUI.removeWindow(window);
	}
	
}